from enum import Enum, auto, unique


@unique
class MeasurementLine(Enum):
    BEGIN_MEASUREMENT__BLOCK = auto()
    NAME__LINE = auto()
    TYPE__LINE = auto()
    L3__LINE = auto()
    L4__LINE = auto()
    LIMIT__LINE = auto()
    L6__LINE = auto()
    BIT__MASK_LINE = auto()
    FUNCTION_LIST__LINE = auto()
    END_MEASUREMENT__BLOCK = auto()
