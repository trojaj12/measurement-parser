import re
import sys
from collections import namedtuple
from typing import Optional, TextIO

from CustomEnums import *

Measure = namedtuple('Measure', 'variable comment type l3 l4 limit l6 bit_mask function_list')
regex = {
    MeasurementLine.BEGIN_MEASUREMENT__BLOCK: r'^\s*\/begin\s+MEASUREMENT\s*$',
    MeasurementLine.NAME__LINE: r'^\s+\w+\s+"[^"]*"\s*$',
    MeasurementLine.TYPE__LINE: r'^\s*\w+\s*$',
    MeasurementLine.L3__LINE: r'^\s*\w+\s*$',
    MeasurementLine.L4__LINE: r'^\s*\d+\s+\d+\s*$',
    MeasurementLine.LIMIT__LINE: r'^\s*\d+.?\d*\s+\d+.?\d*\s*$',
    MeasurementLine.L6__LINE: r'^\s*IF_DATA.*$',
    MeasurementLine.BIT__MASK_LINE: r'^\s*BIT_MASK\s+0x[0123456789abcdefABCDEF]+\s*$',
    MeasurementLine.FUNCTION_LIST__LINE: r'^\s*',
    MeasurementLine.END_MEASUREMENT__BLOCK: r'^\s*\/end\s+MEASUREMENT\s*$',
    }


class EndOfBlock(Exception):
    pass


class MeasurementParser:
    def __init__(self, file: str) -> None:
        self.__try_file_availability(file)
        self.__file = file
        self.__list_of_measurements = dict()

    @staticmethod
    def __try_file_availability(file: str) -> None:
        with open(file, mode="r"):
            pass

    def get_measurement(self, measure_variable: str) -> Measure:
        """
        Gets measurement form file/memory
        :param measure_variable: type/variable of measure
        :return: measurement
        """
        if measure_variable in self.__list_of_measurements:
            return self.__list_of_measurements.get(measure_variable)

        res = self.__get_measurement_from_file(measure_variable)
        self.__list_of_measurements[measure_variable] = res
        return res

    def __get_measurement_from_file(self, measure_variable: str) -> Optional[Measure]:
        """
         Gets measurement form file
        :param measure_variable: type/variable of measure
        :return: measurement
        """
        with open(self.__file, mode='r', encoding="utf8", errors='ignore') as f:
            for line in f:
                if re.match(regex.get(MeasurementLine.BEGIN_MEASUREMENT__BLOCK), line):
                    block = self.__try_parse_block(f, measure_variable)
                    if block is None:
                        continue
                    else:
                        return block
            return None

    @staticmethod
    def __try_parse_block(f: TextIO, measure_variable: str) -> Optional[Measure]:
        # zde je možná až moc striktní požadavek na formát, dalo byse čekat na neprázdný string, returnout necelá data...
        try:
            variable, comment = MeasurementParser.__parse_line(next(f), MeasurementLine.NAME__LINE)
            if variable != measure_variable:
                return None
            type_of_value = MeasurementParser.__parse_line(next(f), MeasurementLine.TYPE__LINE)
            l3 = MeasurementParser.__parse_line(next(f), MeasurementLine.L3__LINE)
            l4 = MeasurementParser.__parse_line(next(f), MeasurementLine.L4__LINE)
            limit = MeasurementParser.__parse_line(next(f), MeasurementLine.LIMIT__LINE)
            l6 = MeasurementParser.__parse_line(next(f), MeasurementLine.L6__LINE)
            bit_mask = MeasurementParser.__parse_line(next(f), MeasurementLine.BIT__MASK_LINE)
            function_list = MeasurementParser.__parse_line(next(f), MeasurementLine.FUNCTION_LIST__LINE)
            return Measure(variable, comment, type_of_value, l3, l4, limit, l6, bit_mask, function_list)
        except EndOfBlock:
            return None

    @staticmethod
    def __parse_line(line: str, measurement_line: MeasurementLine):
        """
        Extract data from string representing line
        :param line:
        :param measurement_line: type of line
        :return: extracted data from line
        """
        if MeasurementParser.__match_line_or_end_block(line, measurement_line):
            line = line.strip()
            return {
                MeasurementLine.NAME__LINE: lambda l: (l[:l.index(" ")], l[l.index('"'):].replace('"', '')),
                MeasurementLine.TYPE__LINE: lambda l: l,
                MeasurementLine.L3__LINE: lambda l: l,
                MeasurementLine.L4__LINE: lambda l: tuple(map(int, l.split())),
                MeasurementLine.LIMIT__LINE: lambda l: tuple(map(float, l.split())),
                MeasurementLine.L6__LINE: lambda l: tuple(l.split()),
                MeasurementLine.BIT__MASK_LINE: lambda l: int(l.split()[1], base=16),
                MeasurementLine.FUNCTION_LIST__LINE: lambda l: l.split()[1:]
                }.get(measurement_line)(line)
        else:
            return None

    @staticmethod
    def __match_line_or_end_block(line: str, measurement_line: MeasurementLine) -> bool:
        """
        Check if line matches type of line or *end of block line*
        :param line:
        :param measurement_line: type of line
        :return: True in case line matches with type fo line, otherwise False
        """
        if re.match(regex.get(measurement_line), line):
            return True
        if re.match(regex.get(MeasurementLine.END_MEASUREMENT__BLOCK), line):
            raise EndOfBlock
        return False


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Extracts measure data from a2l file")
    parser.add_argument("-v", "--variable",
                        action='store',
                        type=str,
                        required=True,
                        help="name of measure variable")
    parser.add_argument("file",
                        help="a2l file for parsing")

    args = parser.parse_args()
    try:
        mp = MeasurementParser(args.file)
        print(mp.get_measurement(args.variable))
    except KeyError as e:
        print(f"{e} is not a valid input.", file=sys.stderr)
        sys.exit(1)
    except IOError as e:
        print(f"Could not read the file properly.", file=sys.stderr)
        sys.exit(2)
    except Exception as e:
        print(f"Something went wrong, please check if the file is in a correct format.", file=sys.stderr)
        sys.exit(100)
    sys.exit(0)
